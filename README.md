# 19级大二第二学期期末考试-Linux高级-工管班-考题

## 考试要求

1. 本次考试由git仓库提交，请勿直接使用当前的试卷，应该复制一份到自己的文件夹，再行修改，否则将导致试卷无效；
2. 选择题答案写在题目后面的小括号中；
3. 试卷满分100分，考试时间2个小时，请注意把握时间；
4. 请填写学号和姓名（或者需要保证答卷文件在以自己姓名命名的目录内），否则可能导致答卷无效；


<font color='red'>

## <p align="right">学号：　　　　　　　　　考生姓名：　　　　　　　　</p>
</font>

## 一、选择题(共60分，共30题,每题2分)

1. 在登录Linux时，一个具有唯一进程ID号的shell将被调用，这个ID是什么()
    ```
    A.NID B.PID C.UID C.CID
    ```

2. 下面那个用户存放用户密码信息()
    ```
    A./boot B./etc C./var D./dev
    ```
3. 用于自动补全功能时，输入命令或文件的前1个或后几个字母按什么键()
    ```
    A.ctrl B.tab C.alt D.esc
    ```
4. vim退出不保存的命令是()
    ```
    A.:q B.q C.:wq D.:q!
    ```
5. 文件权限读、写、执行三种符号的标志依次是()
    ```
    A.rwx B.xrw C.rdx D.rws
    ```
6. 某文件的组外成员的权限是只读、属主是全部权限、组内权限是可读可写、该文件权限为()
    ```
    A.467 B.674 C.476 D.764
    ```
7. 改变文件的属主的命令是()
    ```
    A.chmod B.touch C.chown D.cat
    ```
8. 查找指定进程信息，我们可以用()
    ```
    A.ps -aux | grep filename
    B.ps -aux | man filename
    C.ps -aux | find filename
    D.ps -aux | more filename
    ```
9. Linux配置文件一般放在什么目录()
    ```
    A.etc B.bin C.lib D.dev
    ```
10. linux中查看内存，交换内存的情况命令是()
    ```
    A.top B.last c.free D.lastcomm
    ```
11. 观察系统动态进程的命令是()
    ```
    A.free B.top C.lastcomm D.df
    ```
12. 如果执行命令，chmod 746 file.txt ，那么该文件的权限是()
    ```
    A.rwxr—rw-
    B.rw-r—r—
    C.—xr—rwx
    D.rwxr—r—
    ```
13. 找出当前目录以及其子目录所有扩展名为”.txt”的文件，那么命令是()
    ```
    A.ls .txt
    B.find /opt -name ".txt"
    C.ls -d .txt
    d.find -name "*.txt"
    ```
14. 什么命令常用于检测网络主机是否可达? ()
    ```
    A.ssh B.netstat C.ping D.exit
    ```
15. 退出交互式shell，应该输入什么? ()
    ```
    A:q! B.quit C.; D.exit
    ```
16. 下列文件中，包含了主机名到IP地址映射关系的文件是? ()
    ```
    A./etc/hostname
    B./etc/hosts
    C./etc/resolv.conf
    D./etc/networks
    ```
17. 哪个命令无法查看linux文件内容？ ()
    ```
    A.tac B.more C.head D.man
    ```
18. 使用rm -i 系统会提示什么信息？ ()
    ```
    A.命令所有参数
    B.是否真的删除
    C.是否有写的权限
    D.文件的路径
    ```

19. Linux系统的运行日志存储的目录是。()
    ```
    A./var/log
    B./usr/log
    C./etc/log
    D./tmp/log
    ```

20. Linux系统配置日志可以是在以下哪个文件（或目录）()。
    ```
    A./usr/rsyslog.d
    B./usr/rsyslog.conf
    C./etc/rsyslog.conf
    D./tmp/rsyslog.conf
    ```

21. 在Shell 脚本中，用来读取文件内各个域的内容并将其赋值给Shell 变量的命令是（）。
    ```
    A fold
    B join
    C tr
    D read
    ```
22. 终止一个前台进程可能用到的命令和操作（） 。
    ```
    A kill
    B Ctrl+C
    C shut down
    D halt
    ```
23. 用ls –al 命令列出下面的文件列表，（） 文件是符号连接文件。
    ```
    A -rw-rw-rw- 2 hel-s users 56 Sep 09 11:05 hello
    B -rwxrwxrwx 2 hel-s users 56 Sep 09 11:05 goodbey
    C drwxr–r– 1 hel users 1024 Sep 10 08:10 zhang
    D lrwxr–r– 1 hel users 2024 Sep 12 08:12 cheng
    ```
24. WWW服务器是在Internet 上使用最为广泛的服务，它采用的是（）结构。
    ```
    A 服务器/工作站
    B B/S
    C 集中式
    D 分布式
    ```
24. 在日常管理中，通常CPU 会影响系统性能的情况是：（）。
    ```
    A CPU 已满负荷地运转
    B CPU 的运行效率为30%
    C CPU 的运行效率为50%
    D CPU 的运行效率为80%
    ```

25. DNS 域名系统主要负责主机名和（）之间的解析。
    ```
    A IP 地址
    B MAC 地址
    C 网络地址
    D 主机别名
    ```
26. DHCP 是动态主机配置协议的简称，其作用是可以使网络管理员通过一台服务器来管理一个网络系统，自动地为一个网络中的主机分配（）地址。
    ```
    A 网络
    B MAC
    C TCP
    D IP
    ```
27. 在给定文件中查找与设定条件相符字符串的命令为：（）。
    ```
    A grep
    B gzip
    C find
    D sort
    ```
28. 建立一个新文件可以使用的命令为（）。
    ```
    A chmod
    B more
    C cp
    D touch (指令改变档案的时间记录。)
    ```
29. 在下列命令中，不能显示文本文件内容的命令是：（）。
    ```
    A more
    B less
    C tail
    D join
    ```
30. crontab 文件由六个域组成 ，每个域之间用空格分割，其排列如下：（）。
    ```
    A MIN HOUR DAY MONTH YEAR COMMAND
    B MIN HOUR DAY MONTH DAYOFWEEK COMMAND
    C COMMAND HOUR DAY MONTH DAYOFWEEK
    D COMMAND YEAR MONTH DAY HOUR MIN
    ```
## 二、问答题（共20分，共5题，每题4分）

1. vim有几种工作模式
    ```
    答：
    ```
2. 每月的5,15,25的晚上5点50重启nginx，写下操作过程的各个命令
    ```
    答：
    ```
3. 某文件权限是drw-r—rw-，请解读该权限？
    ```
    答：
    ```
4. linux安装软件的常用方式（以Centos8为例）？
    ```
    答：
    ```
5. 给如下代码添加注释
    ```
    server { 
        listen 80; 
        server_name 9ihub.com; 
        location / { 
        root /var/www/9ihub.com; 
        index index.html;
        }
    }
    ```
## 三、操作题(共1题，满分20分)

将软件开发班完成的微说说应用(https://gitee.com/myhfw003/gradle19_soft2_node_course_Demo)部署到阿里云服务器上，使其可以正常访问
要求：
1. 使用pm2进行部署
2. 使用nginx的反向代理
3. 使用一个单独的三级域名（如soft1.9ihub.com）
4. 部署完成后在下方留下访问地址：